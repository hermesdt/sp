class CreateAdminCollections < ActiveRecord::Migration
  def change
    create_table :collections do |t|
      t.string :name, :null => false
      t.integer :tag_ids, :array => true, :default => [], :null => false
      t.boolean :enabled, :default => true, :null => false

      t.timestamps
    end

    add_index :collections, :tag_ids
  end
end
