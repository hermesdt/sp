class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :name, :null => false
      t.integer :image_data_id, :null => false

      t.timestamps
    end
  end
end
