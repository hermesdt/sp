require "test_helper"

describe TwitterImage do
  let(:twitter_image) {
    TwitterImage.new(url: "http://example.com", twitter_user: twitter_users(:one), tweet_id: "1")
  }

  it "must be valid" do
    twitter_image.must_be :valid?
  end
end
