class Api::CollectionsController < Api::ApiController
  def index
    selected_images = [0]

    @twitter_users = TwitterUser.list_order.enabled
    @collections = Collection.list_order.enabled
    @collections.each do |collection|
      collection.cover = collection.images.
        where("id not in (?)", selected_images).first

      selected_images << collection.cover
    end
  end

  def twitter
    @timeout = Setting.get_images_timeout
    
    if name = params[:name]
      name = name[1..-1] if name.start_with?("@")
      if @user = TwitterUser.enabled.where(:name => name).last
        all = if category = @user.category
          category.twitter_users.enabled.list_order.all
        else
          TwitterUser.enabled.list_order.all
        end

        if (index = all.index(@user)) < all.size - 1
          @next_collection_type = "twitter"
          @next_collection_id = all[index + 1].name
        else
          # if collection = Collection.enabled.list_order.first
          #   @next_collection_type = "collection"
          #   @next_collection_id = collection.id
          # end
        end
        @images = @user.twitter_images.order("tweet_id DESC")
      else
        render :nothing => true, :status => 404
      end
    else
      render :nothing => true, :status => 404
    end
  end

  def images
    @images = []
    @timeout = Setting.get_images_timeout
    @title = ""

    if collection = Collection.enabled.where(:id => params[:id]).first
      @title = collection.name
      @images = collection.images.order("id ASC")

      all = Collection.enabled.list_order.all

      if (index = all.index(collection)) < all.size - 1
        @next_collection_type = "collection"
        @next_collection_id = all[index + 1].id
      end

    end
  end

  def finished
    if !current_user.email.present?
      render :json => {ask_email: true}
    elsif !current_user.premium?
      render :json => {go_premium: true}
    else
      render :json => {go_index: true}
    end
  end

  private
    def get_page_size
      @page_size = 3
    end
end