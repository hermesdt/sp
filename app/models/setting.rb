class Setting < ActiveRecord::Base
  IMAGE_UMBRAL = "image_umbral"
  NEGATIVE_IMAGE_UMBRAL = "negative_image_umbral"
  ASK_EMAIL_COUNT = "ask_email_count"
  MANY_VOTES_EMAIL_1_SUBJECT = "many_votes_email_1_subject"
  MANY_VOTES_EMAIL_1_TEXT = "many_votes_email_1_text"
  MANY_VOTES_EMAIL_1_COUNT = "many_votes_email_1_count"
  IMAGES_TIMEOUT = "images_timeout"

	validates :name, :value, :presence => true
  validates :name, :uniqueness => true

  def self.get_images_timeout
    if timeout = Setting.where(:name => IMAGES_TIMEOUT).first
      timeout.value.to_f
    else
      3
    end
  end

  def self.get_image_umbral
    if umbral = Setting.where(:name => IMAGE_UMBRAL).first
      if match = umbral.value.match(/^(\d+)%$/)
        number = match[1].to_i
        count = User.count
        ((count * number) / 100).to_i
      else
        umbral.value.to_i
      end
    else
      20
    end
  end

  def self.get_negative_image_umbral
    if umbral = Setting.where(:name => NEGATIVE_IMAGE_UMBRAL).first
      if match = umbral.value.match(/^(\d+)%$/)
        number = match[1].to_i
        count = User.count
        ((count * number) / 100).to_i
      else
        umbral.value.to_i
      end
    else
      20
    end
  end

  def self.ask_email_count
    if value = Setting.where(:name => ASK_EMAIL_COUNT).first.try(:value)
      value.to_i
    else
      25
    end
  end

  def self.get_many_votes_email_1_count
    if value = Setting.where(:name => MANY_VOTES_EMAIL_1_COUNT).first.try(:value)
      value.to_i
    else
      100
    end
  end

  def self.get_many_votes_email_1_subject
    value = Setting.where(:name => MANY_VOTES_EMAIL_1_SUBJECT).first.try(:value)
    value ||= ""
  end

  def self.get_many_votes_email_1_text
    value = Setting.where(:name => MANY_VOTES_EMAIL_1_TEXT).first.try(:value)
    value ||= ""
  end
end
