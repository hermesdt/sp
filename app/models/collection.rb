class Collection < ActiveRecord::Base
  validates :name, presence: true

  before_save :sort_tag_ids

  scope :enabled, ->{where(:enabled => true)}
  scope :list_order, ->{order("id DESC")}

  attr_accessor :cover

  def images
    images = Image.all
    images = tag_ids.any? ? images.where("assigned_tag_ids @> '{?}'", self.tag_ids) : images
    images = negative_tag_ids.any? ? images.where("negative_tag_ids @> '{?}'", self.negative_tag_ids) : images

    images
  end

  private
    def sort_tag_ids
      self.tag_ids = self.tag_ids.map(&:to_i)
      self.tag_ids.sort!
      self.tag_ids_will_change!

      self.negative_tag_ids = self.negative_tag_ids.map(&:to_i)
      self.negative_tag_ids.sort!
      self.negative_tag_ids_will_change!
    end
end
