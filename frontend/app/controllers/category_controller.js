'use strict';
var controllers = angular.module('spApp.controllers');
controllers.controller('CategoryController', ['$scope', '$rootScope', '$location', '$http', '$routeParams', function($scope, $rootScope, $location, $http, $routeParams){
    $scope.twitter_users = [];
    $scope.category = {};

    $http.get($rootScope.basePath + '/api/categories/'+ $routeParams.id +'.json')
        .success(function(data){
            $scope.twitter_users = data.twitter_users;
            $scope.category = data.category;
        });
}]);