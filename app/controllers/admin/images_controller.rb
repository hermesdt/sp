class Admin::ImagesController < Admin::AdminController
  before_action :set_image, only: [:show, :edit, :update, :destroy, :remove_tag]

  skip_before_filter :verify_authenticity_token, :only => [:create]

  # GET /admin/images
  # GET /admin/images.json
  def index
    @images = Image.page(params[:page]).per(get_page_size).order("id DESC")
  end

  # GET /admin/images/1
  # GET /admin/images/1.json
  def show
  end

  def remove_tag
    tag_id = params[:tag_id].to_i
    assigned_tag_ids = @image.assigned_tag_ids
    @image.assigned_tag_ids = assigned_tag_ids - [tag_id]
    @image.assigned_tag_ids_will_change!
    @image.save!

    render :js => "window.location.reload();"
  end

  # GET /admin/images/new
  def new
    @image = Image.new
  end

  # GET /admin/images/1/edit
  def edit
  end

  # POST /admin/images
  # POST /admin/images.json
  def create
    @image = Image.new(image_params)
    @image.name = image_params[:image_data_attributes][:raw_data].original_filename
    
    tags = []
    if params[:tags].present? && params[:tags].is_a?(Array)
      tags = params[:tags].map do |tag_name|
        Tag.find_or_create_by!(:name => tag_name)
      end
    end

    @image.assigned_tag_ids = tags.map(&:id)

    respond_to do |format|
      if @image.save
        format.html { redirect_to [:admin, @image], notice: 'Image was successfully created.' }
        format.json { render :show, status: :created, location: [:admin, @image] }
      else
        # image_data.destroy
        format.html { render :new }
        format.json { render json: @image.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/images/1
  # PATCH/PUT /admin/images/1.json
  def update
    @image.name = image_params[:image_data_attributes][:raw_data].original_filename

    respond_to do |format|
      if @image.update(image_params)
        format.html { redirect_to [:admin, @image], notice: 'Image was successfully updated.' }
        format.json { render :show, status: :ok, location: [:admin, @image] }
      else
        format.html { render :edit }
        format.json { render json: @image.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/images/1
  # DELETE /admin/images/1.json
  def destroy
    @image.destroy
    respond_to do |format|
      format.html { redirect_to admin_images_url, notice: 'Image was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_image
      @image = Image.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def image_params
      params.require(:image).permit({:image_data_attributes => [:raw_data]})
    end
end
