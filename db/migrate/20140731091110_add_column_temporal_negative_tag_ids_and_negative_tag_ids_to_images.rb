class AddColumnTemporalNegativeTagIdsAndNegativeTagIdsToImages < ActiveRecord::Migration
  def change
    add_column :images, :temporal_negative_tag_ids, :hstore, :default => {}
    add_column :images, :negative_tag_ids, :integer, :array => true, :default => []
    add_index :images, :temporal_negative_tag_ids
    add_index :images, :negative_tag_ids
  end
end
