'use strict';
var directives = angular.module('spApp.directives', []);
directives.directive('startFullscreen', ['Fullscreen', function(Fullscreen){
	return {
		restrict: 'A',
		link: function(scope, elem){
			elem.on('touchend', Fullscreen.requestFullscreen);
		}
	};
}]);

directives.directive('exitFullscreen', ['Fullscreen', function(Fullscreen){
	return {
		restrict: 'A',
		link: function(scope, elem){
			elem.on('touchend', Fullscreen.exitFullscreen);
		}
	};
}]);

directives.directive('toggleFullscreen', ['Fullscreen', function(Fullscreen){
	return {
		restrict: 'A',
		link: function(scope, elem){
			elem.on('touchend', Fullscreen.toggleFullscreen);
		}
	};
}]);