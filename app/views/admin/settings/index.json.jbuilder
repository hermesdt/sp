json.array!(@settings) do |setting|
  json.extract! setting, :id, :name, :value
  json.url admin_setting_url(setting, format: :json)
end
