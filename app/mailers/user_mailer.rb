class UserMailer < ActionMailer::Base
  default from: 'kim@slideporn.com'

  def many_votes_email_1 user
    @user = user
    mail(to: @user.email,
      subject: Setting.get_many_votes_email_1_subject,
      content_type: "text/html",
      body: Setting.get_many_votes_email_1_text)
  end
end