require 'test_helper'

describe Setting do
  describe "umbral" do
    it "should return 20 if key not exists" do
      Setting.delete_all
      assert Setting.get_image_umbral == 20
    end

    it "should return 100 as fixed number" do
      Setting.delete_all
      Setting.find_or_create_by!(:name => Setting::IMAGE_UMBRAL, :value => "100")
      assert Setting.get_image_umbral == 100
    end

    it "should return 25 using %" do
      Setting.delete_all
      User.delete_all
      100.times{User.create!}
      Setting.find_or_create_by!(:name => Setting::IMAGE_UMBRAL, :value => "25%")
      assert Setting.get_image_umbral == 25
    end
  end
end
