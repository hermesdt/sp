'use strict';
var controllers = angular.module('spApp.controllers');
controllers.controller('RegisterController', ['$scope', '$rootScope', '$location', '$http', function($scope, $rootScope, $location, $http){
	$scope.header = {
		back: '/collections'
	};

	$scope.submit = function(){
		var url = $rootScope.basePath + '/api/users/add_email.json';
		$http
			.put(url, {email: $scope.email})
			.success(function(data){
				if(data.go_premium){
					$location.path('/premium');
				}else{
					$location.path('/collections');
				}
			});

			ga('send', 'event', 'register', 'email_submitted');
			
		return false;
	};
}]);