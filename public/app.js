'use strict';
var app = angular.module('spApp', ['ngRoute', 'ngAnimate', 'ngTouch', 'ipCookie', 'spApp.controllers', 'spApp.services', 'spApp.directives']);
app.config(['$routeProvider', '$httpProvider', function($routeProvider, $httpProvider){
	$routeProvider
	.when('/', {
		templateUrl: 'partials/index.html',
		controller: 'IndexController',
	})
	.when('/t/:name', {
		templateUrl: 'partials/twitter.html',
		controller: 'TwitterController'
	})
	.when('/c/:id', {
		templateUrl: 'partials/category.html',
		controller: 'CategoryController'
	})
	.when('/categories', {
		templateUrl: 'partials/categories.html',
		controller: 'CategoriesController'
	})
	.when('/collections', {
		templateUrl: 'partials/collections.html',
		controller: 'CollectionsController'
	})
	/*.when('/collections/:id', {
		templateUrl: 'partials/show.html',
		controller: 'ShowCollectionController'
	})*/
	/*.when('/tagger', {
		templateUrl: 'partials/tagger.html',
		controller: 'TaggerController'
	})
	.when('/tagger/ask_email', {
		templateUrl: 'partials/ask_email.html',
		controller: 'AskEmailController'
	})
	.when('/tagger/tutorial', {
		templateUrl: 'partials/tutorial.html',
		controller: 'TutorialController'
	})*/
	.when('/premium', {
		templateUrl: 'partials/premium.html',
		controller: 'PremiumController'
	})
	.when('/register', {
		templateUrl: 'partials/register.html',
		controller: 'RegisterController'
	})
	.otherwise({redirectTo: '/'});

	$httpProvider.defaults.withCredentials = true;
}]);

app.run(['$rootScope', '$location', function($rootScope, $location){
	$rootScope.basePath = 'http://backend.dev:3003';
	$rootScope.header = {
		back: '/'
	};

	$rootScope.$on('$routeChangeSuccess', function(){
		ga('set', 'page', $location.path());
		ga('send', 'pageview');
	});
}]);