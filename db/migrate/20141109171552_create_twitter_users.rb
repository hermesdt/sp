class CreateTwitterUsers < ActiveRecord::Migration
  def change
    create_table :twitter_users do |t|
      t.string :name, :null => false
      t.boolean :enabled, :default => false

      t.timestamps
    end

    add_index :twitter_users, :name, :unique => true
  end
end
