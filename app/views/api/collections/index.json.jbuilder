json.twitter_users @twitter_users.each do |twitter_user|
  json.name twitter_user.name =~ /^@/ ? twitter_user.name : "@" + twitter_user.name
  json.count twitter_user.twitter_images.count
  json.image_url twitter_user.twitter_images.order("tweet_id DESC").first.try(:url)
end

json.collections @collections.each do |collection|
	json.id collection.id
	json.name collection.name
	json.count collection.images.count

	if collection.cover.present?
  	json.image_path image_data_path(collection.cover.image_data_id)
  end
end
