class Api::TwitterController < Api::ApiController
  before_filter :check_twitter_authentication, :except => [:create]

  def create
    @authentication = nil
    if (@authentication = current_user.authentication(Authentication::PROVIDERS::TWITTER)).nil?
      @authentication = Authentication.new(:data => auth_hash,
        :provider => Authentication::PROVIDERS::TWITTER)

      current_user.authentications << @authentication
    end
    
    if auth_params["action"].present? && auth_params["image_id"].present?
      client = TwitterClient.instance
      client.access_token = @authentication.data["credentials"]["token"]
      client.access_token_secret = @authentication.data["credentials"]["secret"]

      if image = TwitterImage.where(:id => auth_params["image_id"]).first
        case auth_params["action"]
        when "retweet" then
          client.retweet(image.tweet_id)
        when "fav" then
          client.fav(image.tweet_id)
        end
      end
    end

    if auth_params["redirect_url"].present?
      redirect_to URI.decode(auth_params["redirect_url"])
    else
      render :text => "<script>self.close()</script>", :status => 200
    end
  end

  def retweet
    execute_for_image do |client, image|
      client.retweet!(image.tweet_id)

      render :json => {:message => "Retweeted successfully"}, :status => 201
    end
  end

  def fav
    execute_for_image do |client, image|
      client.fav!(image.tweet_id)

      render :json => {:message => "Favorited successfully"}, :status => 201
    end
  end

  private
    def execute_for_image &block
      if image = TwitterImage.where(:id => params[:image_id]).first
        client = TwitterClient.instance

        client.access_token = @authentication.data["credentials"]["token"]
        client.access_token_secret = @authentication.data["credentials"]["secret"]

        block.call(client, image)
      else
        render :nothing => true, :status => 404
      end
    rescue Twitter::Error::AlreadyRetweeted => e
      render :json => {:error => "Already retweeted"}, :status => 409
    rescue Twitter::Error::AlreadyFavorited => e
      render :json => {:error => "Already favorited"}, :status => 409
    end

    def check_twitter_authentication
      if authentication = current_user.authentication(Authentication::PROVIDERS::TWITTER)
        @authentication = authentication
      else
        render :nothing => true, :status => 401
      end
    end

    def auth_hash
      request.env['omniauth.auth']
    end

    def auth_params
      request.env['omniauth.params']
    end
end