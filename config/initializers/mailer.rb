ActionMailer::Base.delivery_method = :smtp
ActionMailer::Base.perform_deliveries = true
ActionMailer::Base.raise_delivery_errors = true

mailer_config = YAML.load(ERB.new(File.read("#{Rails.root}/config/mailer.yml")).result)

ActionMailer::Base.smtp_settings = {
  :address              => "smtp.office365.com",
  :port                 => 587,
  :domain               => "slideporn.com",
  :user_name            => ENV["EMAIL_USER"] || mailer_config[Rails.env]["email_user"],
  :password             => ENV["EMAIL_PASS"] || mailer_config[Rails.env]["email_pass"],
  :authentication       => "login",
  :enable_starttls_auto => true
}
