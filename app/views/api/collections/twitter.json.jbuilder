json.timeout @timeout
json.title @user.name =~ /^@/ ? @user.name : "@" + @user.name
json.camelcase_title @user.name.gsub("#", "").gsub(" ", "_").camelcase
json.next_collection_id @next_collection_id
json.next_collection_type @next_collection_type
json.category_id @user.category.friendly_id

counter = 0
json.images @images.each_with_index do |image|
	json.id image.id
	json.url image.url
	json.position counter

	counter += 1
end