class TwitterFetcher < Struct.new(:client, :twitter_user)
  PAGE_SIZE = 120
  MAX_PAGES_FETCHED = 10
  MAX_SAVED_IMAGES = 20
  MAX_IMAGES_TO_SAVE_IN_SERVER = 20

  TAG = "sp:twitter:fetch_images"

  def self.fetch!
    client = TwitterClient.instance
    TwitterUser.enabled.each do |user|
      begin
        TwitterFetcher.new(client, user).fetch!
      rescue Twitter::Error => e
        Rails.logger.error "invalid user: #{user.name}"
        yield(e, user) if block_given?
      end
    end
  end

  def fetch!
    options = {}
    if twitter_user.last_tweet_id > 0
      options[:since_id] = twitter_user.last_tweet_id
    end

    options[:count] = PAGE_SIZE

    pages_fetched = 0
    saved_images = 0
    oldest_tweet_id = [0, twitter_user.last_tweet_id].max

    loop do
      tweets = client.user_timeline(twitter_user.name, options)
      oldest_tweet_id = [oldest_tweet_id, tweets.first.id].max if tweets.any?
      Rails.logger.info "[#{TAG}] #{twitter_user.name}: page: #{pages_fetched}, fetched #{tweets.size} records"

      if tweets.any?
        with_images = extract_tweets_with_images(tweets).
          take(MAX_SAVED_IMAGES - saved_images)

        saved_images += save_images(with_images)
        break if saved_images >= MAX_SAVED_IMAGES
        options[:max_id] = tweets.last.id - 1
      else
        break
      end

      pages_fetched += 1
      break if pages_fetched >= MAX_PAGES_FETCHED
    end

    remove_old_images
    twitter_user.update_attributes({last_tweet_id: oldest_tweet_id})
  end

  private
    def save_images tweets
      created = 0
      tweets.each do |image|
        if !twitter_user.twitter_images.exists?(tweet_id: image[:tweet_id])
          twitter_user.twitter_images.create! tweet_id: image[:tweet_id],
            url: image[:url].to_s, text: image[:text]

            created += 1
        end
      end

      Rails.logger.info "[#{TAG}] #{twitter_user.name}: created #{created} records"
      return created
    end

    def extract_tweets_with_images tweets
      tweets.map do |t|
        if !t.retweet? && t.media? && t.media.find{|m| m.is_a?(Twitter::Media::Photo)} &&
            valid_size?(t.media.find{|m| m.is_a?(Twitter::Media::Photo)})
          {tweet_id: t.id,
            url: t.media.find{|m| m.is_a?(Twitter::Media::Photo)}.media_url,
            text: t.text
          }
        else
          nil
        end
      end.compact
    end

    def remove_old_images
      if twitter_user.twitter_images.size > MAX_IMAGES_TO_SAVE_IN_SERVER
        valid_ids = twitter_user.twitter_images.limit(MAX_IMAGES_TO_SAVE_IN_SERVER).order("tweet_id DESC").map(&:id)
        Rails.logger.info "[#{TAG}] #{twitter_user.name}: will remove #{valid_ids.size} records"

        twitter_user.twitter_images.where(["id not in(?)", valid_ids]).destroy_all
      end

      Rails.logger.info "[#{TAG}] #{twitter_user.name}: have #{twitter_user.twitter_images.count} records"
    end

    def valid_size?(media)
      size = media.sizes[:large]
      size ||= media.sizes[:medium]
      size ||= media.sizes[:small]

      size && size.resize == "fit" && size.w <= size.h
    end
end