require "test_helper"

describe Admin::CollectionsController do

  let(:collection) { collections :one }

  it "gets index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:collections)
  end

  it "gets new" do
    get :new
    assert_response :success
  end

  it "creates collection" do
    assert_difference('Collection.count') do
      post :create, collection: { name: collection.name, :tag_ids => [tags(:one).id] }
    end

    assert_redirected_to admin_collection_path(assigns(:collection))
  end

  it "shows collection" do
    get :show, id: collection
    assert_response :success
  end

  it "gets edit" do
    get :edit, id: collection
    assert_response :success
  end

  it "updates collection" do
    put :update, id: collection, collection: { name: collection.name }
    assert_redirected_to admin_collection_path(assigns(:collection))
  end

  it "destroys collection" do
    assert_difference('Collection.count', -1) do
      delete :destroy, id: collection
    end

    assert_redirected_to admin_collections_path
  end

end
