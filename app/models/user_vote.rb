class UserVote < ActiveRecord::Base
  belongs_to :user

  validates :user_id, :presence => true, :uniqueness => true

  def get_array(key)
    if (str = self.votes[key]).present?
      str = str.to_s if str.is_a?(Array)
      str.split(",").map{|l| l.match(/(\d+)/)[1].to_i}
    else
      []
    end
  end

  def get_total_votes
    self.votes.inject(0){|acum, item| acum + item[1].to_s.split(",").size}
  end
end