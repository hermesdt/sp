jQuery(document).ready(function(){
	$("#new_question #tags-filter, .edit_question #tags-filter").on("keyup", function(){
		var query = $(this).val();
		$.getJSON("/admin/tags?filter="+query, function(data){
			$("#filtered-tags").html("");
			for(var i=0;i<data.length;i++){
				var tag = data[i];
				var li = $("<li data-tag-id='"+tag.id+"'>"+tag.name+"</li>");
				$("#filtered-tags").append(li);
			}

			$("#filtered-tags li").click(function(){
				var tagId = $(this).data("tag-id");
				$("#question_tag_id").val(tagId);
				$(".tag-name").html($(this).html());
			});
		});
	});
});