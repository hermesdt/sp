class Question < ActiveRecord::Base
  belongs_to :tag

  validates :text, :tag_id, :presence => :true
end
