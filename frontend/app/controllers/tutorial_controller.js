'use strict';
var controllers = angular.module('spApp.controllers');
controllers.controller('TutorialController', ['$scope', '$rootScope', '$location', 'ipCookie', function($scope, $rootScope, $location, ipCookie){

	if(ipCookie('tagger_tutorial_count') === undefined){
		ipCookie('tagger_tutorial_count', 0, {expires: 365});
	}

	var count = ipCookie('tagger_tutorial_count');
	if(count <= 5){
		ipCookie('tagger_tutorial_count', ++count);
	}else{
		$location.path('/tagger');
	}
}]);