class AddSomeIndexes < ActiveRecord::Migration
  def change
    add_index :user_votes, :user_id
    add_index :users, :email
    add_index :images, :image_data_id
    add_index :images, :temporal_tag_ids
    add_index :images, :assigned_tag_ids
  end
end
