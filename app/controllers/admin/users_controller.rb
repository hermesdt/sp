class Admin::UsersController < Admin::AdminController
  def index
    @users = User.select("distinct(email)").where("email is not null")
  end
end