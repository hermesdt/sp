class Admin::CollectionsController < Admin::AdminController
  before_action :set_collection, only: [:show, :edit, :update, :destroy]

  # GET /admin/collections
  # GET /admin/collections.json
  def index
    @collections = Collection.page(params[:page]).per(get_page_size).order("id DESC")
  end

  # GET /admin/collections/1
  # GET /admin/collections/1.json
  def show
  end

  # GET /admin/collections/new
  def new
    @collection = Collection.new
  end

  # GET /admin/collections/1/edit
  def edit
  end

  # POST /admin/collections
  # POST /admin/collections.json
  def create
    @collection = Collection.new(collection_params)

    respond_to do |format|
      if @collection.save
        format.html { redirect_to [:admin, @collection], notice: 'Collection was successfully created.' }
        format.json { render :show, status: :created, location: @collection }
      else
        format.html { render :new }
        format.json { render json: @collection.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/collections/1
  # PATCH/PUT /admin/collections/1.json
  def update
    respond_to do |format|
      if @collection.update(collection_params)
        format.html { redirect_to [:admin, @collection], notice: 'Collection was successfully updated.' }
        format.json { render :show, status: :ok, location: @collection }
      else
        format.html { render :edit }
        format.json { render json: @collection.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/collections/1
  # DELETE /admin/collections/1.json
  def destroy
    @collection.destroy
    respond_to do |format|
      format.html { redirect_to admin_collections_url, notice: 'Collection was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_collection
      @collection = Collection.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def collection_params
      params[:collection][:negative_tag_ids] ||= []
      params[:collection][:tag_ids] ||= []
      params.require(:collection).permit(:name, :enabled, :tag_ids => [], :negative_tag_ids => [])
    end
end
