require "test_helper"

describe Admin::QuestionsController do

  let(:question) { questions :one }

  it "gets index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:questions)
  end

  it "gets new" do
    get :new
    assert_response :success
  end

  it "creates admin_question" do
    assert_difference('Question.count') do
      post :create, question: { tag_id: question.tag_id, text: question.text }
    end

    assert_redirected_to admin_question_path(assigns(:question))
  end

  it "dont create question because tag_id is not present" do
    assert_difference('Question.count', 0) do
      post :create, question: { text: question.text }
    end

    assert assigns(:question).invalid?
  end

  it "shows admin_question" do
    get :show, id: question
    assert_response :success
  end

  it "gets edit" do
    get :edit, id: question
    assert_response :success
  end

  it "updates admin_question" do
    put :update, id: question, question: { tag_id: question.tag_id, text: question.text + "how are you"}
    assert_redirected_to admin_question_path(assigns(:question))
    assert assigns(:question).text == question.text + "how are you"
  end

  it "destroys admin_question" do
    assert_difference('Question.count', -1) do
      delete :destroy, id: question
    end

    assert_redirected_to admin_questions_path
  end

end
