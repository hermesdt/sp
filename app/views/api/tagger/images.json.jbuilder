json.page @page
json.page_size @page_size
json.next_page @next_page

json.data @rows.each do |row|
  json.image do
    json.id row.image_id
    json.path image_data_path(Image.select("image_data_id").find(row.image_id).image_data_id)
  end
	
  json.question do
    question = Question.find(row.question_id)
    json.id question.id
    json.text question.text
  end
end