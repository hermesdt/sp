json.timeout @timeout
json.title @title
json.camelcase_title @title.gsub("#", "").gsub(" ", "_").camelcase
json.next_collection_id @next_collection_id

counter = 0
json.images @images.each_with_index do |image|
	json.id image.id
	json.path image_data_path(image.image_data_id)
	json.position counter

	counter += 1
end