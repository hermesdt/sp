class AddColumnEnabledToQuestions < ActiveRecord::Migration
  def change
    add_column :questions, :enabled, :boolean, :default => true, :null => false
    add_index :questions, :enabled
  end
end
