class Api::DownloaderController < Api::ApiController
  def show
    if params[:id].present? && params[:url].blank?
      params[:url] = TwitterImage.find_by_id(params[:id]).try(:url)
    end

    uri = URI.parse(params[:url])
    r = Net::HTTP.get_response(uri)

    orig = Magick::Image.from_blob(r.body).last
    watermark = Magick::Image.read("app/assets/images/watermark_h_line.png").last
    watermark = watermark.crop(0, 0, orig.columns * 3, watermark.rows)
    watermark.background_color = "transparent"
    output = orig.composite watermark.rotate(-45), Magick::SouthGravity, 0, -orig.columns/1.2, Magick::AtopCompositeOp

    data = output.to_blob

    response.headers["Content-Length"] = data.length.to_s
    send_data data, type: r["content-type"],
      disposition: ( request.path =~ /\/i\/\d+$/ ? 'inline' : 'attachment'),
      filename: uri.path.split("/")[-1]
  end
end