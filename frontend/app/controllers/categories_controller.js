'use strict';
var controllers = angular.module('spApp.controllers');
controllers.controller('CategoriesController', ['$scope', '$rootScope', '$location', '$http', function($scope, $rootScope, $location, $http){
    $scope.categories = [];

    $http.get($rootScope.basePath + '/api/categories.json')
        .success(function(data){
            $scope.categories = data.categories;
        });
}]);