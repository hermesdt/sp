'use strict';
var controllers = angular.module('spApp.controllers');
controllers.controller('CollectionsController', ['$scope', '$rootScope', '$location', '$http', function($scope, $rootScope, $location, $http){
	$scope.showTagger = false;
	$scope.collections = [];
	$scope.twitter_users = [];

	$http.get($rootScope.basePath + '/api/collections.json')
		.success(function(data){
			$scope.twitter_users = data.twitter_users;
			$scope.collections = data.collections;
			/*for(var i = 0;i< data.collections.length;i++){
				$scope.collections.push(data.collections[i]);
			}*/
			$scope.showTagger = true;
		});
}]);