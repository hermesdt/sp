class Api::TaggerController < Api::ApiController

  PAGE_SIZE = 3

  def vote
    image_id = params[:image_id]
    question_id = params[:question_id]
    answer = params[:answer]

    if (image = Image.where(:id => image_id).first) &&
      (question = Question.where(:id => question_id).first)
      tag = question.tag

      if !current_user.have_voted?(image, question)
        if answer == "yes"
          image.add_vote!(tag)
        elsif answer == "no"
          image.add_negative_vote!(tag)
        end

        current_user.add_vote!(image, question)

        if current_user.email.present? &&
          current_user.get_user_vote.get_total_votes == Setting.get_many_votes_email_1_count  
          send_many_votes_email_1_threaded(current_user)
        end
      end
    end

    if ask_email?
      render :json => {ask_email: true}
    else
      render :nothing => true
    end
  end

  def images
    @rows = find_rows.shuffle

    if @rows.count >= get_page_size
      @next_page = "/api/tagger/images.json?page=1"
    end
  end

  def finished
    if !current_user.email.present?
      render :json => {ask_email: true}
    elsif !current_user.premium?
      render :json => {go_premium: true}
    else
      render :json => {go_index: true}
    end
  end

  private
    def find_rows
      rows = Image.find_by_sql(<<-SQL
          select image_id, question_id from
          (select i.id as image_id, q.id as question_id
            from user_votes uv, questions q, images i
            where uv.id = #{current_user.get_user_vote.id}
              AND q.enabled = true
              AND NOT i.assigned_tag_ids @> ARRAY[q.tag_id]
              AND NOT i.negative_tag_ids @> ARRAY[q.tag_id]
              AND ((uv.votes -> i.id::text) is null
                OR
              ( (uv.votes -> i.id::text) is not null
                  AND
                (uv.votes -> i.id::text) !~ concat('(\\\[|, )', q.id, '(, |\\\])')))
            limit 2500) rows order by random() limit #{get_page_size};
        SQL
        )
    end

    def send_many_votes_email_1 user
      count = 0
      backoff = 0

      while count < 3
        begin
          UserMailer.many_votes_email_1(current_user).deliver
          break
        rescue Exception => e
          count += 1

          Rails.logger.error "ERROR send email"
          Rails.logger.error e.to_s
          Rails.logger.error e.backtrace.join("\n")

          backoff += 1
          sleep backoff * 2
        end
      end
    end

    def send_many_votes_email_1_threaded user
      Thread.new{
        send_many_votes_email_1(user)
      }.run
    end

    def ask_email?
      user_vote = current_user.get_user_vote
      count = user_vote.get_total_votes

      current_user.email.blank? &&
        count > 0 &&
        (count % Setting.ask_email_count) == 0
    end

    def get_page_size
      @page_size = PAGE_SIZE
    end
end