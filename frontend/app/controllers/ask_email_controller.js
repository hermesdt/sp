'use strict';
var controllers = angular.module('spApp.controllers');
controllers.controller('AskEmailController', ['$scope', '$rootScope', '$location', '$http', function($scope, $rootScope, $location, $http){
	$scope.header = {
		back: '/'
	};

	$scope.submit = function(){
		var url = $rootScope.basePath + '/api/users/add_email.json';
		$http
			.put(url, {email: $scope.email})
			.success(function(){
				$location.path('/tagger');
			});

			ga('send', 'event', 'ask_email', 'email_submitted');
			
		return false;
	};
}]);