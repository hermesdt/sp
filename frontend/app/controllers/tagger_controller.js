'use strict';
var controllers = angular.module('spApp.controllers');
controllers.controller('TaggerController', ['$scope', '$rootScope', '$http', '$location', function($scope, $rootScope, $http, $location){
	$scope.tagger = {
		data: []
	};
	$scope.header = {
		back: '/collections'
	};

	$scope.nextPage = $rootScope.basePath + '/api/tagger/images.json?page=1';

	function addNotPresentElements(data){
		if(data.data.length > 0){
				for(var i = 0;i<data.data.length;i++){
					var found = false;
					var remoteElement = data.data[i];
					for(var j = 0;j<$scope.tagger.data.length && !found;j++){
						var localElement = $scope.tagger.data[j];

						if(localElement !== undefined &&
							remoteElement.image.id === localElement.image.id &&
							remoteElement.question.id === localElement.question.id){
							found = true;
						}
					}
					if(!found){
						$scope.tagger.data.push(remoteElement);
					}
				}
			}
	}
	
	$scope.fetchMoreImages = function(){
		if($scope.tagger.data.length === 0 && $scope.nextPage === null){
			taggerFinished();
		}else{
			if($scope.nextPage !== null){
				$http({method: 'GET', url: $scope.nextPage})
					.success(function(data){
						if(data.ask_email === true){
							$location.path('/tagger/ask_email');
						}else{
							if(data.next_page === null){
								$scope.nextPage = null;
							}else{
								$scope.nextPage = $rootScope.basePath + data.next_page;
							}
							addNotPresentElements(data);
						}
					});
			}
		}
	};

	function taggerFinished(){
		$http.get($rootScope.basePath + '/api/tagger/finished.json').
				success(function(data){
					if(data.ask_email){
						$location.path('/register');
					}else if(data.go_premium){
						$location.path('/premium');
					}else{
						$location.path('/');
					}
				});
			ga('send', 'event', 'tagger', 'images', 'finished');
	}

	$scope.vote = function(answer){
		/*if($location.search().simulate === 'finished'){
			taggerFinished();
			return;
		}*/

		var elem = $scope.tagger.data[0];
		var url = $rootScope.basePath + '/api/tagger/vote';
		$http.post(url,{'image_id': elem.image.id,
			'question_id': elem.question.id, 'answer': answer})
		.success(function(data){
			if(data.ask_email === true){
				$location.path('/tagger/ask_email');
			}
		});

		$scope.tagger.data.splice(0, 1);
		if($scope.tagger.data.length <= 2){
			$scope.fetchMoreImages();
		}

		ga('send', 'event', 'tagger', 'button', answer);
	};

	$scope.fetchMoreImages($scope.nextPage);
}]);