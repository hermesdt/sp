class Category < ActiveRecord::Base
  extend FriendlyId

  friendly_id :name, use: [:slugged, :history]
  
  validates :name, presence: true

  has_many :twitter_users, dependent: :nullify

  def should_generate_new_friendly_id?
    name_changed?
  end

  def cover_url
    self.twitter_users.enabled.first.try(:twitter_images).try(:first).try(:url)
  end
end