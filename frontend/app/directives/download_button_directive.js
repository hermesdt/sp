'use strict';
var directives = angular.module('spApp.directives');
directives.directive('downloadImage', ['$rootScope', 'ApiService', function($rootScope, ApiService){

    var link = function(scope, elem){
        elem.on('click', function(event){
            event.preventDefault();
            var url = scope.collection.images[0].url;
            ApiService.download(url);
            ga('send', 'event', 'twitter', 'download', url);

            return false;
        });
    };

    return {
        restrict: 'E',
        link: link,
        template: '<a class="download fa fa-download" href="#">'+
        '<span>&nbsp;Download</span>'+
        '</a>'
    };
}]);