require 'test_helper'

describe Image do
  it "should add vote 1 votes once" do
    image = images(:without_votes)
    tag = tags(:one)

    assert image.temporal_tag_ids[tag.id.to_s].nil?
    image.add_vote! tag
    image.reload

    assert image.temporal_tag_ids[tag.id.to_s] == "1"
  end

  it "should add negative vote 1 votes once" do
    image = images(:without_votes)
    tag = tags(:one)

    assert image.temporal_negative_tag_ids[tag.id.to_s].nil?
    image.add_negative_vote! tag
    image.reload

    assert image.temporal_negative_tag_ids[tag.id.to_s] == "1"
  end

  it "should promote temporal tag id to assigned tag id" do
    image = images(:without_votes)
    tag = tags(:one)

    image.temporal_tag_ids[tag.id.to_s] = Setting.get_image_umbral
    image.temporal_tag_ids_will_change!
    image.save

    assert image.assigned_tag_ids.empty?
    image.add_vote! tag
    image.reload

    assert image.temporal_tag_ids[tag.id.to_s].nil?
    assert image.assigned_tag_ids.include?(tag.id)
  end

  it "should save assigned tag ids sorted" do
    image = images(:one)
    image.assigned_tag_ids = ["1", "100", "2", "200", "10"]
    image.save!
    image.reload

    assert image.assigned_tag_ids == [1, 2, 10, 100, 200]
  end

  it "should save negative tag ids sorted" do
    image = images(:one)
    image.negative_tag_ids = ["1", "100", "2", "200", "10"]
    image.save!
    image.reload

    assert image.negative_tag_ids == [1, 2, 10, 100, 200]
  end

  it "should promote negative temporal tag id to negative tag id" do
    image = images(:without_votes)
    tag = tags(:one)

    image.temporal_negative_tag_ids[tag.id.to_s] = Setting.get_negative_image_umbral
    image.temporal_negative_tag_ids_will_change!
    image.save

    assert image.negative_tag_ids.empty?
    image.add_negative_vote! tag
    image.reload

    assert image.temporal_negative_tag_ids[tag.id.to_s].nil?
    assert image.negative_tag_ids.include?(tag.id)
  end
end
