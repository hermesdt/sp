class CreateTwitterImages < ActiveRecord::Migration
  def change
    create_table :twitter_images do |t|
      t.references :twitter_user, :null => false
      t.integer :tweet_id, :limit => 8, :null => false
      t.text :url, :null => false

      t.timestamps
    end

    add_index :twitter_images, :tweet_id, :unique => true
    add_index :twitter_images, :twitter_user_id
  end
end
