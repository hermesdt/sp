class User < ActiveRecord::Base
  has_one :user_vote, :dependent => :destroy
  has_many :authentications, :dependent => :destroy

  def authenticated?(provider)
    self.authentication(provider).present?
  end

  def authentication(provider)
    self.authentications.where(:provider => provider).first
  end

  def have_voted?(image, question)
    return get_user_vote.get_array(image.id.to_s).include?(question.id)
  end

  def add_vote!(image, question)
    if !have_voted?(image, question)
      user_vote = get_user_vote

      user_vote.votes[image.id.to_s] = 
        user_vote.get_array(image.id.to_s) << question.id

      user_vote.votes_will_change!
      user_vote.save!
    end
  end

  def get_user_vote
    @user_vote ||= UserVote.find_or_create_by!(:user_id => self.id)
  end
end
