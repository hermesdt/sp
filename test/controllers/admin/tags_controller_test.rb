require 'test_helper'

class Admin::TagsControllerTest < ActionController::TestCase
  setup do
    @tag = tags(:one)
  end

  test "should get index with all tags" do
    get :index
    assert_response :success
    assert Tag.count == assigns(:tags).count
  end

  test "should get index with filtered tags" do
    get :index, {:filter => "tits"}
    assert_response :success
    assert Tag.count != assigns(:tags)
    
    assert assigns(:tags).select{|t| t.name.match("tits")}.
      count == assigns(:tags).count
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create admin_tag" do
    assert_difference('Tag.count') do
      post :create, tag: { name: @tag.name }
    end

    assert_redirected_to admin_tag_path(assigns(:tag))
  end

  test "should show admin_tag" do
    get :show, id: @tag
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @tag
    assert_response :success
  end

  test "should update admin_tag" do
    patch :update, id: @tag, tag: { name: @tag.name }
    assert_redirected_to admin_tag_path(assigns(:tag))
  end

  test "should not destroy tag because have associated questions" do
    assert @tag.questions.count > 0
    assert_difference('Tag.count', 0) do
      delete :destroy, id: @tag
    end

    assert_redirected_to admin_tags_path
  end

  test "should destroy tag because have no associated objects" do
    tag = Tag.create!(:name => "some tag")
    assert_difference('Tag.count', -1) do
      delete :destroy, id: tag
    end
    
    assert_redirected_to admin_tags_path
  end
end
