describe("TaggerController", function(){
	var controller, scope, httpBackend;

	beforeEach(module('spApp.controllers'));
	beforeEach(inject(function($controller, $rootScope, $httpBackend){
		$rootScope.basePath = "http://localhost:3000";
		scope = $rootScope.$new();
		httpBackend = $httpBackend;
		controller = $controller('TaggerController', {$scope: scope});
	}));

	it("should have nextPage defined", function(){
		expect(scope.nextPage).toEqual("http://localhost:3000/api/tagger/images.json?page=1");
	});

	it("should filter repeated elements", function(){
		httpBackend.when("GET", scope.nextPage).
		respond({
			page: 1, page_size: 3, next_page: null, data: [
				{image: {id: 1, name: "some name"}, question: {id: 1}},
				{image: {id: 1, name: "some name"}, question: {id: 1}},
				{image: {id: 1, name: "some name"}, question: {id: 1}},
				{image: {id: 1, name: "some name"}, question: {id: 2}},
				{image: {id: 50, name: "some name"}, question: {id: 1}}
			]});
		
		httpBackend.flush();
		expect(scope.tagger.data.length).toEqual(3);
		for(var i = 0; i<scope.tagger.data.length;i++){
			var foundCounter = 0;
			for(var j = 0;j<scope.tagger.data.length;j++){
				var d1 = scope.tagger.data[i],
				d2 = scope.tagger.data[j];
				if(d1.image.id == d2.image.id && d1.question.id == d2.question.id){
					foundCounter += 1;
				}
			}
			expect(foundCounter).toEqual(1);
		}
	});
});