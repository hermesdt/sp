'use strict';
var services = angular.module('spApp.services', []);
services.factory('Fullscreen', function(){
	var fullscreen = {};

	fullscreen.isFullscreen = function(){
		return !!(document.fullscreenElement ||    // alternative standard method
			      document.mozFullScreenElement ||
			      document.webkitFullscreenElement || document.msFullscreenElement);
	};

	fullscreen.requestFullscreen = function(){
		if (!fullscreen.isFullscreen()) {  // current working methods
		  	if (document.documentElement.requestFullscreen) {
		  		document.documentElement.requestFullscreen();
			} else if (document.documentElement.msRequestFullscreen) {
				document.documentElement.msRequestFullscreen();
			} else if (document.documentElement.mozRequestFullScreen) {
				document.documentElement.mozRequestFullScreen();
			} else if (document.documentElement.webkitRequestFullscreen) {
				document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
			}
		}
	};

	fullscreen.exitFullscreen = function(){
		if (fullscreen.isFullscreen()) {  // current working methods
	      	if (document.exitFullscreen) {
		  		document.exitFullscreen();
		  	} else if (document.msExitFullscreen) {
		  		document.msExitFullscreen();
		  	} else if (document.mozCancelFullScreen) {
		  		document.mozCancelFullScreen();
		  	} else if (document.webkitExitFullscreen) {
		  		document.webkitExitFullscreen();
		  	}
		}
	};
	fullscreen.toggleFullscreen = function(){
		if(fullscreen.isFullscreen()){
			fullscreen.exitFullscreen();
		}else{
			fullscreen.requestFullscreen();
		}
	};

	return fullscreen;
});