'use strict';
var directives = angular.module('spApp.directives');
directives.directive('shareTwitterButton', [function(){

	var link = function(scope, elem){
		var url = 'https://twitter.com/share';

		elem.on('click', function(evt){
			var text = elem.attr('share-text');
			var shareUrl = elem.attr('share-url');

			url += '?text=' + encodeURIComponent(text) +
				'&original_referer=' + encodeURIComponent('http://slideporn.com');

			if(shareUrl){
				url += '&url=' + encodeURIComponent(shareUrl);
			}
			evt.preventDefault();
			window.open(url, 'twitter', 'width=500,height=500,left=100,top=100');
			ga('send', 'event', 'twitter', 'button', 'share');
			return false;
		});
	};

	return {
		restrict: 'A',
		link: link
	};
}]);