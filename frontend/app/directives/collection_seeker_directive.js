'use strict';
var directives = angular.module('spApp.directives');
directives.directive('collectionSeeker', [function(){

	var link = function(scope, elem){
		elem.on('mouseup', function(event){
			scope.$apply(function(){
				scope.seek(elem.val());
			});
		});

		elem.on('touchend', function(event){
			scope.$apply(function(){
				scope.seek(elem.val());
			});
		});
	};

	return {
		restrict: 'A',
		link: link
	};
}]);