class Api::UsersController < Api::ApiController
  def add_email
    if current_user && current_user.email.blank?
      if @success = current_user.update_attributes({:email => params[:email]})
        return render :status => :ok, :json => {go_premium: !current_user.premium?}
      end
    end

    render :nothing => true, :status => 500
  end
end