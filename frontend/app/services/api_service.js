'use strict';
var services = angular.module('spApp.services');
services.service('ApiService', ['$http', '$q', '$rootScope', '$window', function($http, $q, $rootScope, $window){

	var basePath = $rootScope.basePath;

	var api = {
		retweet: function(collectionId, imageId){
			var deferred = $q.defer();
			$http.get(basePath + '/api/twitter/retweet/' + imageId)
			.success(function(data, status){
				deferred.resolve(data);
			})
			.error(function(rejection, status){
				var url = '#/t/' + collectionId + '?image_id=' + imageId;
				var encodedUrl = encodeURIComponent(url);
				if(status === 401){
					// window.open(basePath + '/auth/twitter?action=retweet&image_id=' + imageId + '&url=' + encodedUrl, 'twitter', 'width=500,height=500,left=100,top=100');
					window.location = '/auth/twitter?action=retweet&image_id=' + imageId + '&redirect_url=' + encodedUrl;
				}else{
					deferred.reject(rejection);
				}
			});

			return deferred.promise;
		},
		fav: function(collectionId, imageId){
			var deferred = $q.defer();
			$http.get(basePath + '/api/twitter/fav/' + imageId)
			.success(function(data, status){
				deferred.resolve(data);
			})
			.error(function(rejection, status){
				var url = '#/t/' + collectionId + '?image_id=' + imageId;
				var encodedUrl = encodeURIComponent(url);
				if(status === 401){
					// window.open(basePath + '/auth/twitter?action=fav&image_id=' + imageId + '&url=' + encodedUrl, 'twitter', 'width=500,height=500,left=100,top=100');
					window.location = '/auth/twitter?action=fav&image_id=' + imageId + '&redirect_url=' + encodedUrl;
				}else{
					deferred.reject(rejection);
				}
			});

			return deferred.promise;
		},
		download: function(url){
			var fullUrl = basePath + '/api/downloader?url='+encodeURIComponent(url);
			if(navigator.userAgent.match(/i(Pad|Phone)/)){
				window.open(fullUrl, 'download', 'width=500,height=500,left=100,top=100');
			}else{
				window.location.href = fullUrl;
			}
			
			/*$http.get()
			.success(function(data, status){
			});*/
		}
	};

	return api;
}]);