window.ga = function(){};

describe("ShowCollectionController", function(){
	var createController, scope, httpBackend, _ipCookie, _createTimeout;

	beforeEach(module('spApp.controllers'));
	beforeEach(module('ipCookie'));

	beforeEach(inject(function($controller, $rootScope, $httpBackend, ipCookie){
		$rootScope.basePath = "http://localhost:3000";
		scope = $rootScope.$new();
		httpBackend = $httpBackend;
		_ipCookie = ipCookie;

		createController = function(id){
			var ctr = $controller('ShowCollectionController',
				{$scope: scope, $routeParams: {id: 555}, ipCookie: ipCookie});
			_createTimeout = ctr.createTimeout;
			ctr.createTimeout = function(){console.log('create called');};

			return ctr;
		};
	}));

	beforeEach(inject(function($httpBackend){
		$httpBackend.when("GET", "http://localhost:3000/api/collections/555/images.json?padding=0").
			respond({
				page: 1,
				page_size: 3,
				next_page: "http://localhost:3000/api/collections/555/images.json?padding=0&page=2",
				timeout: 3,
				title: "some title",
				count: 10,
				padding: 0,
				images: [
					{id: 1, path: "/image_data/1", position: 1},
					{id: 2, path: "/image_data/2", position: 2},
					{id: 3, path: "/image_data/3", position: 3},
				]
			});
	}));

	afterEach(function() {
		httpBackend.verifyNoOutstandingExpectation();
		httpBackend.verifyNoOutstandingRequest();
	});

	it("should not toggle pause becuase not learned", function(){
		var controller = createController();
		httpBackend.flush();

		scope.collection.paused = false;
		scope.collection.togglePause();

		expect(scope.collection.paused).toEqual(false);
	});

	it("should toggle pause", function(){
		var controller = createController();
		httpBackend.flush();

		scope.collection.paused = false;
		scope.collection.playLearned = 'yes';
		scope.collection.togglePause();

		expect(scope.collection.paused).toEqual(true);
	});

	it("should initial value to nextpage as: /api/collections/555/images.json?padding=0", function(){
		var controller = createController();
		expect(scope.collection.nextPage).
			toEqual("/api/collections/555/images.json?padding=0");

		httpBackend.flush();

		expect(scope.collection.nextPage).
			toEqual("http://localhost:3000/api/collections/555/images.json?padding=0&page=2");


		expect(scope.collection.images.length).toEqual(3);
	});
});