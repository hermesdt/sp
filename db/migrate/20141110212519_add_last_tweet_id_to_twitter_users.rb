class AddLastTweetIdToTwitterUsers < ActiveRecord::Migration
  def change
    add_column :twitter_users, :last_tweet_id, :integer, :limit => 8, :default => 0
  end
end
