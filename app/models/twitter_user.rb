class TwitterUser < ActiveRecord::Base
  after_create :fetch_images
  validates :name, :presence => true
  validates :name, :uniqueness => true

  has_many :twitter_images, dependent: :destroy
  belongs_to :category

  scope :enabled, ->{where(enabled: true)}
  scope :list_order, ->{order("id DESC")}

  private
    def fetch_images
      begin
        TwitterFetcher.new(TwitterClient.instance, self).fetch!
      rescue Exception => e
        self.errors[:base] << "#{e.class} - #{e.to_s}"
        raise e
      end
    end
end
