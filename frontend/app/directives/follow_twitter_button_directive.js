'use strict';
var directives = angular.module('spApp.directives');
directives.directive('followTwitterButton', [function(){

	var link = function(scope, elem){
		var url = 'https://twitter.com/intent/follow?screen_name=slideporn';

		elem.on('click', function(evt){
			evt.preventDefault();
			window.open(url, 'twitter', 'width=500,height=500,left=100,top=100');
			ga('send', 'event', 'twitter', 'button', 'follow');
			return false;
		});
	};

	return {
		restrict: 'A',
		link: link,
		template: 	'<a id="follow-twitter-button">'+
						'<span class="fa fa-twitter"></span>'+
						'<span class="text">Follow me!</span>'+
				   	'</a>',
		replace: true
	};
}]);