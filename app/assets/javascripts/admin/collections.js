jQuery(document).ready(function(){
	$("#new_collection #tags-filter, .edit_collection #tags-filter, "+
		"#new_collection #negative-tags-filter, .edit_collection #negative-tags-filter").on("keyup", function(){
		var query = $(this).val()
		, elem = $(this);

		$.getJSON("/admin/tags?filter="+query, function(data){
			elem.next(".filtered-tags").html("");
			for(var i=0;i<data.length;i++){
				var tag = data[i];
				var li = $("<li data-tag-id='"+tag.id+"'>"+tag.name+"</li>");
				elem.next(".filtered-tags").append(li);
			}

			elem.next(".filtered-tags").find("li").click(function(){
				var tagId = $(this).data("tag-id");
				var hidden_name, hidden_id, target;

				if(elem.attr("id") == "tags-filter"){
					if($(".selected-tags li.tag[data-tag-id='"+tagId+"']").length > 0){
						return;
					}

					hidden_name = "collection[tag_ids][]";
					hidden_id = "collection_tag_ids_";
					target = ".selected-tags";
				}else if(elem.attr("id") == "negative-tags-filter"){
					if($(".negative-selected-tags li.tag[data-tag-id='"+tagId+"']").length > 0){
						return;
					}

					hidden_name = "collection[negative_tag_ids][]";
					hidden_id = "collection_negative_tag_ids_";
					target = ".negative-selected-tags";
				}

				var hiddenElement = $("<input type='hidden' value='"+tagId+"' name='"+hidden_name+"' id='"+hidden_id+""+tagId+"' />");
				var removeElement = $("<a href='#' class='remove-tag' data-tag-id='"+tagId+"'>X</a>");
				var newTag = $("<li class='tag' data-tag-id='"+tagId+"'>"+$(this).html()+"</li>");
				newTag.append(removeElement).append(hiddenElement);

				$(target).append(newTag);

				$(".selected-tags .remove-tag, .negative-selected-tags .remove-tag").on("click", function(){
					$(this).closest(".tag").remove();
					return false;
				});
			});
		});
	});

	$(".selected-tags .remove-tag, .negative-selected-tags .remove-tag").on("click", function(){
		$(this).closest(".tag").remove();
		return false;
	});
});