class ImageDatasController < ApplicationController
  def show
    image_data = ImageData.find(params[:id])

    response.headers["Cache-Control"] = "max-age=31536000"
    response.headers["Expires"] = (image_data.updated_at + 1.years).httpdate

    send_data(image_data.data, :type => image_data.content_type)
  end
end