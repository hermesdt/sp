'use strict';
var controllers = angular.module('spApp.controllers', []);
controllers.controller('IndexController', ['$location', 'ipCookie', function($location, ipCookie){
	if(ipCookie('indexVisited') === 'yes'){
		$location.path('/categories');
	}else{
		ipCookie('indexVisited', 'yes', {expires: 365});
	}
}]);