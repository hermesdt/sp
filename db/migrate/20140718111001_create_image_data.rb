class CreateImageData < ActiveRecord::Migration
  def change
    create_table :image_data do |t|
      t.binary :data, :null => false
      t.string :content_type, :null => false

      t.timestamps
    end
  end
end
