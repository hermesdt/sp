json.category do
  json.name @category.name
  json.id @category.friendly_id
end

json.twitter_users @twitter_users.each do |twitter_user|
  json.name twitter_user.name =~ /^@/ ? twitter_user.name : "@" + twitter_user.name
  json.count twitter_user.twitter_images.count
  json.image_url twitter_user.twitter_images.order("tweet_id DESC").first.try(:url)
end