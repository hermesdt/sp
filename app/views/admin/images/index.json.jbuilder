json.array!(@images) do |admin_image|
  json.extract! admin_image, :id, :digest
  json.url admin_image_url(admin_image, format: :json)
end
