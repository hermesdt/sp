'use strict';
var controllers = angular.module('spApp.controllers');
controllers.controller('ShowCollectionController', ['$scope', '$rootScope', '$timeout', '$routeParams', '$location', '$http', 'ipCookie', function($scope, $rootScope, $timeout, $routeParams, $location, $http, ipCookie){
	var id = $routeParams.id;
	var nextCollectionId = null;

	$scope.header = {
		back: '/collections'
	};

	$scope.collection = {
		showControls: $routeParams.showControls === 'true',
		started: false,
		images: [],
		fetchedImages: [],
		timeout: 3,
		paused: $routeParams.started !== 'true',
		title: '',
		camelcase_title: '',
		currentPosition: 0
	};
	var lastTimeout = null;

	fetchImages();
	if(!$scope.collection.paused){
		createTimeout($scope.collection.timeout * 1000);
	}

	$scope.collection.toggleControlsVisibility = function(){
		if($scope.collection.paused){
			$scope.collection.showControls = !$scope.collection.showControls;
		}else{
			$scope.collection.showControls = false;
			$scope.collection.togglePause();
		}
	};

	$scope.collection.togglePause = function(){
		$scope.collection.paused = !$scope.collection.paused;
		if($scope.collection.paused){
			$timeout.cancel(lastTimeout);
			ga('send', 'event', 'collection_'+id, 'image_'+$scope.collection.images[0].id, 'paused');
		}else{
			$scope.collection.showControls = false;
			if($scope.collection.images.length <= 2){
				addBatch();
				removeFirstImage();
			}else{
				removeFirstImage();
			}
			createTimeout($scope.collection.timeout * 1000);
			ga('send', 'event', 'collection_'+id, 'image_'+$scope.collection.images[0].id, 'resumed');
		}
	};

	function addBatch(index){
		if(index === undefined){
			index = parseInt($scope.collection.images[$scope.collection.images.length - 1].position) + 1;
		}else{
			index = parseInt(index);
		}
		for(var i = index;i<$scope.collection.fetchedImages.length && $scope.collection.images.length < 5;i++){
			$scope.collection.images.push($scope.collection.fetchedImages[i]);
		}
	}

	$scope.seek = function(value){

		/*$timeout.cancel(lastTimeout);*/
		value = parseInt(value);
		if(value < 0){ value = 0;}
		if(value > $scope.collection.fetchedImages.length - 1){value = $scope.collection.fetchedImages.length - 1;}
		$scope.collection.images.splice(0, $scope.collection.images.length);
		addBatch(value);
		updateCurrentPosition();
		/*createTimeout($scope.collection.timeout * 1000);
		$scope.collection.paused = false;*/

		ga('send', 'event', 'collection_'+id, 'images', 'seeked');
	};

	function fetchImages(){
		$http.get($rootScope.basePath + '/api/collections/'+id+'/images.json')
			.success(function(data){
				$scope.collection.title = data.title;
				$scope.collection.camelcase_title = data.camelcase_title;
				$scope.collection.timeout = parseFloat(data.timeout);
				$scope.collection.count = data.images.length;
				$scope.collection.fetchedImages = data.images;
				nextCollectionId = data.next_collection_id;

				if($scope.collection.fetchedImages.length > 0){
					for(var i = 0;i<3;i++){
						$scope.collection.images.push($scope.collection.fetchedImages[i]);
					}
					$scope.collection.currentPosition = $scope.collection.images[0].position;
				}
			});
	}

	function collectionFinished(){
		if(nextCollectionId !== null){
			$location.url('/collections/' + nextCollectionId + '?started=' + !$scope.collection.paused + '&showControls=' + $scope.collection.showControls);
		}else{
			$http.get($rootScope.basePath + '/api/collections/finished.json').
				success(function(data){
					if(data.ask_email){
						$location.path('/register');
					}else if(data.go_premium){
						$location.path('/premium');
					}else{
						$location.path('/');
					}
				});
			ga('send', 'event', 'collection_'+id, 'images', 'finished');
		}
	}

	function createTimeout(delay){
		$timeout.cancel(lastTimeout);
		lastTimeout = $timeout(nextImage(delay), delay);
	}

	function nextImage(delay){
		return function(){

			if($scope.collection.images.length <= 2){
				if($scope.collection.currentPosition < $scope.collection.fetchedImages.length - 1){
					addBatch();
					removeFirstImage();
					createTimeout(delay);
				}else{
					collectionFinished();
				}
			}else{
				removeFirstImage();
				createTimeout(delay);
			}
		};
	}

	function removeFirstImage(){
		$scope.collection.images.splice(0, 1);
		if($scope.collection.images.length > 0){
			updateCurrentPosition();
		}else{
			$scope.collection.currentPosition = $scope.collection.count;
		}
	}

	function updateCurrentPosition(){
		$scope.collection.currentPosition = $scope.collection.images[0].position;
	}

	$scope.swipeLeft = function(){
		if($scope.collection.currentPosition === $scope.collection.count - 1){
			$timeout.cancel(lastTimeout);
			collectionFinished();
		}else{
			$scope.collection.images.splice(0, $scope.collection.images.length);
			addBatch(parseInt($scope.collection.currentPosition) + 1);
			updateCurrentPosition();
			if(!$scope.collection.paused){
				createTimeout($scope.collection.timeout * 1000);
			}
		}

		ga('send', 'event', 'collection_'+id, 'images', 'swipeLeft');
	};

	$scope.swipeRight = function(){
		if($scope.collection.currentPosition === 0){
			return;
		}else{
			$scope.collection.images.splice(0, $scope.collection.images.length);
			addBatch(parseInt($scope.collection.currentPosition) - 1);
			updateCurrentPosition();
			if(!$scope.collection.paused){
				createTimeout($scope.collection.timeout * 1000);
			}
		}
		ga('send', 'event', 'collection_'+id, 'images', 'swipeRight');
	};

	$scope.$on('$destroy', function(){
		if(lastTimeout){
			$timeout.cancel(lastTimeout);
		}
		window.removeEventListener('keyup', keylistener);
	});

	var keylistener = function(evt){
		$scope.$apply(function(){
			switch(evt.keyCode){
				case 37:
					$scope.swipeRight();
					break;
				case 39:
					$scope.swipeLeft();
					break;
				case 32:
					$scope.collection.togglePause();
					break;
			}
		});
	};

	window.addEventListener('keyup', keylistener);
}]);