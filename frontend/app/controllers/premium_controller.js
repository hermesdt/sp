'use strict';
var controllers = angular.module('spApp.controllers');
controllers.controller('PremiumController', ['$scope', '$rootScope', '$location', function($scope, $rootScope, $location){
	$scope.header = {
		back: '/collections'
	};

	$scope.goPremiumClick = function(){
		ga('send', 'event', 'premium', 'buy_button', 'clicked');
	};
}]);