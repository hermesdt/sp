class CreateTableCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :name, null: false
      t.timestamps
    end

    add_column :twitter_users, :category_id, :integer
    add_foreign_key :twitter_users, :categories
  end
end
