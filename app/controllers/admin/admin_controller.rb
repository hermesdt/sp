class Admin::AdminController < ApplicationController
  http_basic_authenticate_with name: "sprules", password: "$sprules$" if Rails.env.production?

  layout "admin"

  private
    def get_page_size
      15
    end
end