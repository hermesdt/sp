namespace :sp do
  desc "remove old invalid users"
  task :remove_invalid_users => :environment do
    users = User.where(email: nil).where("created_at < ?", Date.today - 10.days)
    users.select{|u| u.authentications.size == 0}.each{|u| u.destroy}
  end

  namespace :twitter do
    desc "fetch images"
    task :fetch_images => :environment do
      TwitterFetcher.fetch!
    end
  end

  desc "upload files"
  task :upload do
    extra_tags = Proc.new{|file_name|
      file_name = file_name.gsub("images/upload/", "")
      tag_names = file_name.split("/")
      if tag_names.size > 1
        tag_names[0..-2]
      else
        []
      end
    }

    Rails.env = "production" if ENV["RAILS_ENV"].blank?
    require 'rest-client'
    require 'fileutils'

    uploads = Dir.glob("images/upload/**/*")
    puts "Nothing to upload" and return if uploads.empty?

    uploads.each do |file_name|
      next if !File.file?(file_name)
      f = File.new(file_name, "rb")

      begin
        url = if Rails.env.development?
          "http://backend.dev:3003/admin/images"
        else
          "http://sliderp.herokuapp.com/admin/images"
        end

        tags = extra_tags.call(file_name)
        r = RestClient::Request.new(
          :method => :post,
          :url => url,
          :user => "sprules",
          :password => "$sprules$",
          :headers => {accept: :json},
          :payload => {image: {image_data_attributes: {raw_data: f}}, :tags => tags}
        )
        r.execute

        puts "done: #{file_name}"
        FileUtils.mv file_name, "images/done"
      rescue Exception => e
        begin
          json = JSON.parse(e.response)
          puts "\nERROR (#{f.path}): #{json.map{|k,v| "#{k}: #{v}"}.join(", ")}\n"
        rescue Exception => json_error
          puts "\nERROR (#{f.path}): Unkown [#{e.http_code}]\n"
        end
      end
    end
  end
end
