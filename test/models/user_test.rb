require 'test_helper'

describe User do
  let(:user){ User.create! }

  it "should create user_vote when calling get_user_vote" do
    assert_difference "UserVote.count" do
      user.get_user_vote
    end
  end

  it "should return false to have voted" do
    assert !user.have_voted?(images(:one), questions(:one))
  end

  it "should return true to have voted" do
    user.get_user_vote.votes = {}
    user.get_user_vote.save!

    user.add_vote!(images(:one), questions(:one))
    user.get_user_vote.reload
    assert user.get_user_vote.votes[images(:one).id.to_s] == "[#{questions(:one).id}]"
    assert user.have_voted?(images(:one), questions(:one))
  end
end