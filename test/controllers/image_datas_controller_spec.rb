require 'test_helper'

describe ImageDatasController do
  fixtures :image_datas
  
  before do
  end

  describe "when send request to show without headers" do
    it "should return 200 and image bytes" do
      image_data = image_datas(:one)
      get :show, id: image_data
      assert_response 200, image_data.data
      assert Time.parse(response.headers["Expires"]).is_a? Time
    end
  end
end