'use strict';
/* not used by now */
var services = angular.module('spApp.services');
services.factory('Session', ['$rootScope', '$http', function($rootScope, $http){
	var session = {};
	session.shouldAskEmailValue = null;
	session.sessionDataFetched = false;

	session.shouldAskEmail = function(cb){
		fetchSessionData(function(){
			cb(session.shouldAskEmailValue);
		});
	};
	session.setShouldAskEmail = function(value){
		session.setShouldAskEmail = value;
	};

	var fetchSessionData = function(cb){
		if(session.sessionDataFetched === false){
			$http.get($rootScope.basePath + '/api/session_data.json').
				success(function(data){
					for(var key in data){
						session[key] = data[key];
					}
					session.sessionDataFetched = true;
					cb();
				});
		}else{
			cb();
		}
	};

	return session;
}]);