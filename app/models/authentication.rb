class Authentication < ActiveRecord::Base
  belongs_to :user

  module PROVIDERS
    TWITTER = "twitter"
  end
end
