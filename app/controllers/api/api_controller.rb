class Api::ApiController < ApplicationController
  skip_before_filter :verify_authenticity_token
  before_filter :login_filter
  before_filter :cors

  private
    def cors
      if Rails.env.development?
        headers['Access-Control-Allow-Origin'] = 'http://frontend.dev:3000'
        headers['Access-Control-Allow-Methods'] = 'POST, PUT, DELETE, GET, OPTIONS'
        headers['Access-Control-Request-Method'] = '*'
        headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
        headers['Access-Control-Allow-Credentials'] = 'true'
      end
    end

    def login_filter
      if !logged_in?
        @current_user = User.create!
        session[:user_id] = @current_user.id
      end
    end

    def current_user
      if @current_user.nil?
        @current_user = User.where(:id => session[:user_id]).first
        if @current_user.nil?
          @current_user = User.create!
          session[:user_id] = @current_user.id
          @current_user
        else
          @current_user
        end
      else
        @current_user
      end
    end

    def logged_in?
      session[:user_id].present?
    end

    def get_page
      page = params[:page].to_i
      @page = page <= 0 ? 1 : page
    end

    def get_page_size
      @page_size = 10
    end
end