describe("CollectionsController", function(){
	var createController, scope, httpBackend;

	beforeEach(module('spApp.controllers'));
	beforeEach(inject(function($controller, $rootScope, $httpBackend){
		$rootScope.basePath = "http://localhost:3000";
		scope = $rootScope.$new();
		httpBackend = $httpBackend;
		createController = function(){
			return $controller('CollectionsController', {$scope: scope});
		};
	}));

	afterEach(function() {
		httpBackend.verifyNoOutstandingExpectation();
		httpBackend.verifyNoOutstandingRequest();
	});

	it("should filter repeated elements", function(){
		httpBackend.when("GET", scope.basePath + '/api/collections.json').
		respond({
			collections: [
				{id: 1, name: "collection 1", count: 5, image_path: "/image_data/1"},
				{id: 2, name: "collection 1", count: 10, image_path: "/image_data/2"},
				{id: 3, name: "collection 1", count: 4, image_path: "/image_data/3"},
				{id: 4, name: "collection 1", count: 50, image_path: "/image_data/4"}
			]});
		
		var controller = createController();
		httpBackend.flush();
		expect(scope.collections.length).toEqual(4);
	});
});