class Admin::RootController < Admin::AdminController
  def index
  end

  def reset_all_votes_current_user
    if current_user = User.where(:id => session[:user_id]).first
      user_vote = current_user.get_user_vote
      user_vote.votes = {};
      user_vote.save!
    end

    render :nothing => true
  end

  def reset_email_current_user
    if current_user = User.where(:id => session[:user_id]).first
      current_user.email = nil
      current_user.save!
    end

    render :nothing => true
  end
end