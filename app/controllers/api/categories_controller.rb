class Api::CategoriesController < Api::ApiController
  def index
    selected_images = [0]

    @categories = Category.all
  end

  def show
    if @category = Category.friendly.find(params[:id])
      @twitter_users = @category.twitter_users.list_order.enabled
    else
      render nothing: true, status: 404
    end
  end
end
