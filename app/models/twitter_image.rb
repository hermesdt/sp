class TwitterImage < ActiveRecord::Base
  validates :twitter_user, :tweet_id, :url, :presence => true
  belongs_to :twitter_user

end
