require 'test_helper'

class Admin::SettingsControllerTest < ActionController::TestCase
  setup do
    @setting = settings(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:settings)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should not create setting because have same name" do
    assert_difference('Setting.count', 0) do
      post :create, setting: { name: @setting.name, value: @setting.value }
    end

    assert_response :success
  end

  test "should create setting" do
    assert_difference('Setting.count') do
      post :create, setting: { name: @setting.name + " some data", value: @setting.value }
    end

    assert_redirected_to admin_setting_path(assigns(:setting))
  end

  test "should show admin_setting" do
    get :show, id: @setting
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @setting
    assert_response :success
  end

  test "should update admin_setting" do
    patch :update, id: @setting, setting: { name: @setting.name, value: @setting.value }
    assert_redirected_to admin_setting_path(assigns(:setting))
  end

  test "should destroy admin_setting" do
    assert_difference('Setting.count', -1) do
      delete :destroy, id: @setting
    end

    assert_redirected_to admin_settings_path
  end
end
