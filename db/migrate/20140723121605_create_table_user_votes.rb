class CreateTableUserVotes < ActiveRecord::Migration
  def change
    create_table :user_votes do |t|
      t.integer :user_id, :null => false
      t.hstore :votes, :default => {}, :null => false

      t.timestamps
    end
  end

  def up
    sql <<-SQL
    CREATE OR REPLACE FUNCTION json_array_int(json_arr json) RETURNS int[]
    LANGUAGE plpgsql IMMUTABLE AS $$
    DECLARE
        rec int;
        len int;
        ret int[];
    BEGIN
        BEGIN
            len := json_array_length(json_arr);
        EXCEPTION
            WHEN OTHERS THEN
                RETURN ret;
        END;

        FOR rec IN SELECT elem::text::int FROM  json_array_elements(json_arr) AS elem
        LOOP
            ret := array_append(ret,rec);
        END LOOP;
        RETURN ret;
    END $$;
    SQL

    execute sql

  end

  def down
  end
end
