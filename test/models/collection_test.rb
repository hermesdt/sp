require "test_helper"

describe Collection do
  let(:collection) { Collection.new(:name => "name 1", :tag_ids => [tags(:one).id]) }

  it "must be valid" do
    collection.must_be :valid?
  end

  it "must return only enabled collections" do
    Collection.destroy_all

    Collection.new(:name => "a", :enabled => false, :tag_ids => [tags(:one).id]).save!
    Collection.new(:name => "a", :enabled => false, :tag_ids => [tags(:one).id]).save!
    Collection.new(:name => "a", :enabled => false, :tag_ids => [tags(:one).id]).save!

    Collection.new(:name => "a", :enabled => true, :tag_ids => [tags(:one).id]).save!
    Collection.new(:name => "a", :enabled => true, :tag_ids => [tags(:one).id]).save!
    Collection.new(:name => "a", :enabled => true, :tag_ids => [tags(:one).id]).save!

    assert Collection.enabled.count == 3
  end
end
