json.array!(@tags) do |tag|
  json.extract! tag, :id, :name
  json.url admin_tag_url(tag, format: :json)
end
