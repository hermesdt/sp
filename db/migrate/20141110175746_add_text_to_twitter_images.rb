class AddTextToTwitterImages < ActiveRecord::Migration
  def change
    add_column :twitter_images, :text, :text, :null => false
  end
end
