require 'test_helper'

describe Api::CollectionsController do
  def data
    Rack::Test::UploadedFile.new(Rails.root.join('test/fixtures/github.png'), 'image/png')
  end

  let(:image_data){ImageData.create!(:raw_data => data, :content_type => "image/png")}
  
  it "should only return 3 enabled collections" do
    Collection.destroy_all

    Collection.new(:name => "a", :enabled => false, :tag_ids => [tags(:one).id]).save!
    Collection.new(:name => "a", :enabled => false, :tag_ids => [tags(:one).id]).save!
    Collection.new(:name => "a", :enabled => false, :tag_ids => [tags(:one).id]).save!

    Collection.new(:name => "a", :enabled => true, :tag_ids => [tags(:one).id, tags(:two).id]).save!
    Collection.new(:name => "a", :enabled => true, :tag_ids => [tags(:one).id]).save!
    Collection.new(:name => "a", :enabled => true, :tag_ids => [tags(:one).id, tags(:two).id]).save!

    get :index, format: :json
    assert assigns(:collections).count == 3
    assert assigns(:collections).inject(true){|acum, item| acum &= item.enabled?}
  end

  it "should only return 1 collection with 2 images" do
    Collection.destroy_all
    Collection.new(:name => "a", :enabled => true, :tag_ids => [tags(:one).id],
      :negative_tag_ids => [tags(:two).id]).save!

    get :index, format: :json
    assert assigns(:collections).count == 1

    Image.destroy_all
    10.times {|i|
      if i <= 3
        Image.create!(:name => "my new name #{i}", :image_data_id => image_data.id,
          :assigned_tag_ids => [tags(:one).id])
      elsif i <= 7
        Image.create!(:name => "my new name #{i}", :image_data_id => image_data.id,
          :negative_tag_ids => [tags(:two).id])
      else
        Image.create!(:name => "my new name #{i}", :image_data_id => image_data.id,
          :assigned_tag_ids => [tags(:one).id], :negative_tag_ids => [tags(:two).id])
      end

    }

    images = assigns(:collections)[0].images
    assert_equal images.count, 2
  end

  it "should return 9 images" do
    tag = Tag.create!(:name => "my new tag")
    collection = Collection.create!(:name => "my new collection", :tag_ids => [tag.id])
    Image.destroy_all
    9.times {|i|
        Image.create!(:name => "my new name #{i}", :image_data_id => image_data.id, :assigned_tag_ids => [tag.id])
    }

    get :images, format: :json, id: collection.id
    assert_equal assigns(:images).size, 9
  end

  it "should respond ask_email true" do
    u = User.create!
    session[:user_id] = u.id
    get :finished
    assert_equal JSON.parse(response.body), {"ask_email" => true}
  end

  it "should respond premium true" do
    u = User.create!(:email => "someemail@mail.com")
    session[:user_id] = u.id
    get :finished
    assert_equal JSON.parse(response.body), {"go_premium" => true}
  end

  it "should respond go index true" do
    u = User.create!(:email => "someemail@mail.com", :premium => true)
    session[:user_id] = u.id
    get :finished
    assert_equal JSON.parse(response.body), {"go_index" => true}
  end
end
