class Tag < ActiveRecord::Base
  has_many :images, :through => :image_tags
  has_many :questions

  validates :name, :presence => true

  before_destroy :check_questions_presence

  private
    def check_questions_presence
      self.questions.empty?
    end
end
