require 'test_helper'

describe Api::TaggerController do
  describe "with tables empty" do
    before :each do
      @v, $VERBOSE = $VERBOSE, nil
      clear_data
      assert Image.count == 0
    end

    after :each do
      $VERBOSE = @v
    end

    def data
      Rack::Test::UploadedFile.new(Rails.root.join('test/fixtures/github.png'), 'image/png')
    end

    let(:image_data){ImageData.create!(:raw_data => data, :content_type => "image/png")}
    let(:image){Image.create!(:name => "name 1", :image_data_id => image_data.id)}
    let(:tag){Tag.create!(:name => "blondes")}
    let(:question){Question.create!(:text => "text of the question", :tag_id => tag.id)}

    it "should respond with 7 rows for a user with 9 votes, for a total 16 possible votes" do
      clear_data
      4.times{ |i|
        image_data = ImageData.create!(:raw_data => data, :content_type => "image/png")
        image = Image.create!(:name => "randome name #{i}", :image_data_id => image_data.id)
        tag = Tag.create!(:name => "blondes #{i}")
        question = Question.create!(:text => "text of the question", :tag_id => tag.id)
      }

      user = users(:one)
      session[:user_id] = user.id

      user.add_vote! Image.order("id ASC").all[0], Question.order("id ASC").all[0]
      user.add_vote! Image.order("id ASC").all[0], Question.order("id ASC").all[1]
      user.add_vote! Image.order("id ASC").all[0], Question.order("id ASC").all[2]
      user.add_vote! Image.order("id ASC").all[1], Question.order("id ASC").all[0]
      user.add_vote! Image.order("id ASC").all[1], Question.order("id ASC").all[1]
      user.add_vote! Image.order("id ASC").all[1], Question.order("id ASC").all[2]
      user.add_vote! Image.order("id ASC").all[2], Question.order("id ASC").all[2]
      user.add_vote! Image.order("id ASC").all[2], Question.order("id ASC").all[1]
      user.add_vote! Image.order("id ASC").all[2], Question.order("id ASC").all[0]

      Api::TaggerController.stub_any_instance(:get_page_size, 1000) do
        get :images, format: :json
        assert assigns(:rows).size == 7
      end
    end

    it "should return empty rows array if nothing exists" do
      clear_data
      get :images, format: :json
      assert assigns(:rows).size == 0
    end

    it "should return 1 row" do
      _, _ = image, question
      get :images, format: :json
      assert assigns(:rows).size == 1
    end

    it "should return 3 rows" do
      clear_data
      3.times{ |i|
        image_data = ImageData.create!(:raw_data => data, :content_type => "image/png")
        image = Image.create!(:name => "randome name #{i}", :image_data_id => image_data.id)
        tag = Tag.create!(:name => "blondes #{i}")
        question = Question.create!(:text => "text of the question", :tag_id => tag.id)
      }

      get :images, format: :json
      assert assigns(:rows).size == 3
    end

    it "should return 3 rows" do
      clear_data
      4.times{ |i|
        image_data = ImageData.create!(:raw_data => data, :content_type => "image/png")
        image = Image.create!(:name => "randome name #{i}", :image_data_id => image_data.id)
        tag = Tag.create!(:name => "blondes #{i}")
        question = Question.create!(:text => "text of the question", :tag_id => tag.id, :enabled => i > 1)
      }

      prev_value = Api::TaggerController::PAGE_SIZE
      Api::TaggerController::PAGE_SIZE = 100

      get :images, format: :json
      assert_equal assigns(:rows).size, 8

      Api::TaggerController::PAGE_SIZE = prev_value
    end

    it "should return 1 rows" do
      clear_data
      tag = Tag.create!(:name => "blondes")
      question = Question.create!(:text => "text of the question", :tag_id => tag.id)
      3.times{ |i|
        image_data = ImageData.create!(:raw_data => data, :content_type => "image/png")
        image = Image.create!(:name => "name #{i}", :image_data_id => image_data.id)

        if i == 1
          image.assigned_tag_ids << question.tag_id
          image.assigned_tag_ids_will_change!
          image.save!
        end

        if i == 2
          image.negative_tag_ids << question.tag_id
          image.negative_tag_ids_will_change!
          image.save!
        end
      }

      get :images, format: :json
      assert assigns(:rows).size == 1
    end

    it "should return true to should ask email?" do
      user = User.create!
      session[:user_id] = user.id
      user_vote = user.get_user_vote
      user_vote.votes["1"] = (1..Setting.ask_email_count).to_a
      user_vote.votes_will_change!
      user_vote.save!

      post :vote
      assert JSON.parse(response.body)["ask_email"]
    end

    it "should add a temporal tag id to the image" do
      Setting.find_or_create_by!(:name => Setting::IMAGE_UMBRAL, :value => "10")

      assert image.temporal_tag_ids[question.tag.id.to_s].to_i == 0
      post :vote, {question_id: question.id, image_id: image.id, answer: "yes"}
      image.reload
      assert image.temporal_tag_ids[question.tag.id.to_s].to_i == 1
    end

    it "should add a assigned tag id to the image" do
      Setting.find_or_create_by!(:name => Setting::IMAGE_UMBRAL, :value => "1")

      assert !image.assigned_tag_ids.include?(question.tag.id)
      post :vote, {question_id: question.id, image_id: image.id, answer: "yes"}
      image.reload
      assert image.assigned_tag_ids.include?(question.tag.id)
    end

    it "should not add a assigned tag id to the image, and add to negative tag ids" do
      Setting.find_or_create_by!(:name => Setting::IMAGE_UMBRAL, :value => "1")
      Setting.find_or_create_by!(:name => Setting::NEGATIVE_IMAGE_UMBRAL, :value => "1")

      assert !image.assigned_tag_ids.include?(question.tag.id)
      post :vote, {question_id: question.id, image_id: image.id, answer: "no"}
      image.reload
      assert !image.assigned_tag_ids.include?(question.tag.id)
      assert image.negative_tag_ids.include?(question.tag.id)
    end

    it "should reassign a new user id" do
      session[:user_id] = -2
      assert_difference "User.count", 1 do
        assert !image.assigned_tag_ids.include?(question.tag.id)
        post :vote, {question_id: question.id, image_id: image.id, answer: "no"}
      end

      assert_difference "User.count", 0 do
        assert !image.assigned_tag_ids.include?(question.tag.id)
        post :vote, {question_id: question.id, image_id: image.id, answer: "no"}
      end

      assert_equal session[:user_id], User.last.id
    end

    private

      def clear_data
        Image.delete_all
        Tag.delete_all
        Question.delete_all
      end
  end
end
