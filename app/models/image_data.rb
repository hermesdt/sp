class ImageData < ActiveRecord::Base
  has_one :image

  validates :data, :content_type, :presence => true
  validate :validate_data_geometry

  def raw_data= input
    self.data = input.read
    self.content_type = input.content_type
  end

  private
    def validate_data_geometry
      return if !new_record?
      image = Magick::Image.from_blob(self.data).first
      if image.columns > image.rows
        self.errors.add :data, "Invalid geometry, must be height > width"
      end
    end
end
