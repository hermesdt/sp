json.categories @categories.each_with_index do |category|
	json.id category.friendly_id
	json.name category.name
	json.image_url category.cover_url
end
