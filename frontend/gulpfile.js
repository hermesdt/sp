/**
 *
 *  Web Starter Kit
 *  Copyright 2014 Google Inc. All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 *
 */

'use strict';

// Include Gulp & Tools We'll Use
var gulp = require('gulp');
var Glue = require('gluejs');
var $ = require('gulp-load-plugins')();
var del = require('del');
var runSequence = require('run-sequence');
var browserSync = require('browser-sync');
var pagespeed = require('psi');
var reload = browserSync.reload;

var AUTOPREFIXER_BROWSERS = [
  'ie >= 10',
  'ie_mob >= 10',
  'ff >= 30',
  'chrome >= 34',
  'safari >= 7',
  'opera >= 23',
  'ios >= 7',
  'android >= 4.4',
  'bb >= 10'
];

// Lint JavaScript
gulp.task('jshint', function () {
  return gulp.src(['app/**/*.js', '!app/vendor/**', '!app/**/*_test.js'])
    .pipe(reload({stream: true, once: true}))
    .pipe($.jshint())
    .pipe($.jshint.reporter('jshint-stylish'))
    .pipe($.if(!browserSync.active, $.jshint.reporter('fail')));
});

// Optimize Images
gulp.task('images', function () {
  return gulp.src('app/assets/images/**/*')
    .pipe($.cache($.imagemin({
      progressive: true,
      interlaced: true
    })))
    .pipe(gulp.dest('dist/assets/images'))
    .pipe($.size({title: 'images'}));
});

// Copy All Files At The Root Level (app)
gulp.task('copy', function () {
  return gulp.src(['app/*','!app/*.html'], {dot: true})
    .pipe(gulp.dest('dist'))
    .pipe($.size({title: 'copy'}));
});

// Copy Web Fonts To Dist
gulp.task('fonts', function () {
  return gulp.src(['app/assets/fonts/**'])
    .pipe(gulp.dest('dist/assets/fonts'))
    .pipe($.size({title: 'fonts'}));
});


// Automatically Prefix CSS
gulp.task('styles:css', function () {
  return gulp.src('app/assets/styles/**/*.css')
    .pipe($.changed('app/assets/styles'))
    .pipe($.autoprefixer(AUTOPREFIXER_BROWSERS))
    .pipe(gulp.dest('app/assets/styles'))
    .pipe($.size({title: 'styles:css'}));
});

// Compile Any Other Sass Files You Added (app/styles)
gulp.task('styles:scss', function () {
  return gulp.src(['app/assets/styles/main.scss'])
    .pipe($.rubySass({
      style: 'expanded',
      precision: 10,
      loadPath: ['app/assets/styles']
    }))
    .on('error', console.error.bind(console))
    .pipe($.autoprefixer(AUTOPREFIXER_BROWSERS))
    .pipe(gulp.dest('.tmp/assets/styles'))
    .pipe($.size({title: 'styles:scss'}));
});

// Output Final CSS Styles
gulp.task('styles', ['styles:scss', 'styles:css']);

// Scan Your HTML For Assets & Optimize Them
gulp.task('html', function () {
  return gulp.src('app/**/*.html')
    .pipe($.useref.assets({searchPath: '{.tmp,app}'}))
    // Concatenate And Minify JavaScript
    .pipe($.if('*.js', $.replace(/http:\/\/192.168.1.20:3003/, '')))
    .pipe($.if('*.js', $.replace(/http:\/\/localhost:3003/, '')))
    .pipe($.if('*.js', $.replace(/http:\/\/backend.dev:3003/, '')))
    .pipe($.if('*.js', $.replace(/\$httpProvider.defaults.withCredentials = true;/, '')))
    .pipe($.if('*.js', $.uglify({preserveComments: 'some'})))
    // Remove Any Unused CSS
    /*.pipe($.if('*.css', $.uncss({
      html: [
        'app/index.html'
      ]
    })))*/
    // Concatenate And Minify Styles
    .pipe($.if('*.css', $.csso()))
    .pipe($.rev())
    .pipe($.useref.restore())
    .pipe($.useref())
    .pipe($.revReplace())
    // Minify Any HTML
    .pipe($.if('*.html', $.minifyHtml({empty: true, quotes: true})))
    // Output Files
    .pipe(gulp.dest('dist'))
    .pipe($.size({title: 'html'}));
});

gulp.task('clean', ['clean:frontend', 'clean:backend']);

// Clean Output Directory
gulp.task('clean:frontend', del.bind(null, ['.tmp', 'dist']));
gulp.task('clean:backend', del.bind(null, ['../public/assets/*/{main,app}.min-*.{js,css}'], {force: true}));

// Watch Files For Changes & Reload
gulp.task('serve', function () {
  browserSync({
    notify: false,
    server: {
      baseDir: ['.tmp', 'app', 'app/vendor'],
    },
  });

  gulp.watch(['app/**/*.html'], reload);
  gulp.watch(['app/assets/styles/**/*.scss'], ['styles:scss']);
  gulp.watch(['{.tmp,app}/assets/styles/**/*.css'], ['styles:css', reload]);
  gulp.watch(['app/**/*.js'], ['jshint']);
  gulp.watch(['app/assets/images/**/*'], reload);
});

// Build and serve the output from the dist build
gulp.task('serve:dist', ['default'], function () {
  browserSync({
    notify: false,
    server: {
      baseDir: 'dist'
    }
  });
});

// Build Production Files, the Default Task
gulp.task('default', ['clean'], function (cb) {
  runSequence('styles', ['jshint', 'html', 'images', 'fonts', 'copy'], 'copy:rails', cb);
});

gulp.task('copy:rails', function(){
  return gulp.src(['dist/**'], {dot: true})
    .pipe(gulp.dest('../public'))
    .pipe($.size({title: 'copy:rails'}));
});

// Run PageSpeed Insights
// Update `url` below to the public URL for your site
gulp.task('pagespeed', pagespeed.bind(null, {
  // By default, we use the PageSpeed Insights
  // free (no API key) tier. You can use a Google
  // Developer API key if you have one. See
  // http://goo.gl/RkN0vE for info key: 'YOUR_API_KEY'
  url: 'https://example.com',
  strategy: 'mobile'
}));

gulp.task('test:karma', function(){
  var testFiles = [
    'app/vendor/angular.min.js',
    'app/vendor/angular-route.min.js',
    'app/vendor/angular-touch.min.js',
    'app/vendor/angular-animate.min.js',
    'app/vendor/angular-mocks.js',
    'app/vendor/angular-cookie.min.js',
    'app/app.js',
    'app/index/index_controller.js',
    'app/tagger/tagger_controller.js',
    'app/collections/collections_controller.js',
    'app/collections/show_controller.js',
    '!app/vendor',
    'test/**/*.js'
  ];

  return gulp.src(testFiles)
    .pipe($.karma({
      configFile: 'karma.conf.js',
      action: 'run'
    }))
    .on('error', function(err) {
      // Make sure failed tests cause gulp to exit non-zero
      throw err;
    });
});

gulp.task('test:e2e', function(){
  browserSync({
    notify: false,
    server: {
      baseDir: ['.tmp', 'app', 'app/vendor']
    }
  });

  return gulp.src('e2e/**/*_spec.js')
    .pipe($.protractor.protractor({
        configFile: 'protractor.conf.js',
    }))
    .on('error', function(e){
      // browserSync.exit();
      throw e;
    })
    .on('end', function(){
      browserSync.exit();
    });
});

gulp.task('test', function(cb){
  runSequence('test:karma', 'test:e2e', cb);
});

// Load custom tasks from the `tasks` directory
try { require('require-dir')('tasks'); } catch (err) {}
