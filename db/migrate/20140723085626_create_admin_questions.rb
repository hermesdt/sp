class CreateAdminQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.string :text, :null => false
      t.integer :tag_id, :null => false

      t.timestamps
    end
  end
end
