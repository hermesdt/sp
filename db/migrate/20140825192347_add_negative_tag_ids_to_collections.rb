class AddNegativeTagIdsToCollections < ActiveRecord::Migration
  def change
    add_column :collections, :negative_tag_ids, :integer, :array => true, :default => [], :null => false
  end
end
