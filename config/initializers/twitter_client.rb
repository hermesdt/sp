class TwitterClient
  def self.instance
    if Rails.configuration.respond_to?(:twitter_client)
      client = Rails.configuration.twitter_client
    else
      twitter_config = YAML.load(ERB.new(File.read("#{Rails.root}/config/twitter.yml")).result)
      client = Twitter::REST::Client.new do |config|
        config.consumer_key        = ENV["TWITTER_KEY"] || twitter_config[Rails.env]["consumer_key"]
        config.consumer_secret     = ENV["TWITTER_SECRET"] || twitter_config[Rails.env]["consumer_secret"]
      end

      Rails.configuration.twitter_client = client
    end

    client
  end
end