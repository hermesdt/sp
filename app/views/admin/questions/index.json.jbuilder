json.array!(@questions) do |question|
  json.extract! question, :id, :text
  json.tag do
    json.id question.tag_id
    json.name question.tag.name
  end
  json.url admin_question_url(question, format: :json)
end
