# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Setting.create(name: Setting::IMAGE_UMBRAL, :value => "20")
Setting.create(name: Setting::NEGATIVE_IMAGE_UMBRAL, :value => "20")
Setting.create(name: Setting::ASK_EMAIL_COUNT, :value => "25")
Setting.create(name: Setting::MANY_VOTES_EMAIL_1_SUBJECT, :value => "subject of first email voting")
Setting.create(name: Setting::MANY_VOTES_EMAIL_1_TEXT, :value => "text of first email voting")
Setting.create(name: Setting::MANY_VOTES_EMAIL_1_COUNT, :value => "100")
Setting.create(name: Setting::IMAGES_TIMEOUT, :value => "3")