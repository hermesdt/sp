class AddTemporalAndAssignedTagsToImages < ActiveRecord::Migration
  def up
    enable_extension :hstore

    add_column :images, :assigned_tag_ids, :integer, array: true, default: [], :null => false
    add_column :images, :temporal_tag_ids, :hstore, default: {}, :null => false
  end

  def down
    remove_column :images, :assigned_tag_ids
    remove_column :images, :temporal_tag_ids

    disable_extension :hstore
  end
end
