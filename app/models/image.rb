class Image < ActiveRecord::Base
  has_many :tags, :through => :image_tags
  belongs_to :image_data, :dependent => :destroy

  validates :name, :image_data, :presence => true
  validates :name, :uniqueness => :true

  accepts_nested_attributes_for :image_data

  before_save :sort_assigned_tag_ids, :sort_negative_tag_ids

  def add_vote! tag
    count = self.temporal_tag_ids[tag.id.to_s].to_i
    count ||= 0
    count += 1

    if !self.assigned_tag_ids.include?(tag.id)
      if count >= Setting.get_image_umbral
        self.temporal_tag_ids.delete(tag.id.to_s)
        self.temporal_tag_ids_will_change!

        self.assigned_tag_ids << tag.id
        self.assigned_tag_ids_will_change!
      else
        self.temporal_tag_ids[tag.id.to_s] = count
        self.temporal_tag_ids_will_change!
      end

      self.save!
    end
  end

  def add_negative_vote! tag
    count = self.temporal_negative_tag_ids[tag.id.to_s].to_i
    count ||= 0
    count += 1

    if !self.negative_tag_ids.include?(tag.id)
      if count >= Setting.get_negative_image_umbral
        self.temporal_negative_tag_ids.delete(tag.id.to_s)
        self.temporal_negative_tag_ids_will_change!

        self.negative_tag_ids << tag.id
        self.negative_tag_ids_will_change!
      else
        self.temporal_negative_tag_ids[tag.id.to_s] = count
        self.temporal_negative_tag_ids_will_change!
      end

      self.save!
    end
  end

  private
    def sort_assigned_tag_ids
      self.assigned_tag_ids = self.assigned_tag_ids.map(&:to_i).sort
      self.assigned_tag_ids_will_change!
    end

    def sort_negative_tag_ids
      self.negative_tag_ids = self.negative_tag_ids.map(&:to_i).sort
      self.negative_tag_ids_will_change!
    end
end
