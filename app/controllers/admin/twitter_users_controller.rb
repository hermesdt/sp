class Admin::TwitterUsersController < Admin::AdminController
  before_action :set_twitter_user, only: [:show, :edit, :update, :destroy]

  # GET /admin/twitter_users
  # GET /admin/twitter_users.json
  def index
    @twitter_users = TwitterUser.page(params[:page]).per(get_page_size).order("id DESC")
  end

  # GET /admin/twitter_users/1
  # GET /admin/twitter_users/1.json
  def show
  end

  # GET /admin/twitter_users/new
  def new
    @twitter_user = TwitterUser.new
  end

  # GET /admin/twitter_users/1/edit
  def edit
  end

  # POST /admin/twitter_users
  # POST /admin/twitter_users.json
  def create
    @twitter_user = TwitterUser.new(twitter_user_params)

    respond_to do |format|
      if @twitter_user.save
        format.html { redirect_to [:admin, @twitter_user], notice: 'twitter_user was successfully created.' }
        format.json { render :show, status: :created, location: @twitter_user }
      else
        format.html { render :new }
        format.json { render json: @twitter_user.errors, status: :unprocessable_entity }
      end
    end
  rescue Exception => e
    respond_to do |format|
      format.html { render :new }
      format.json { render json: @twitter_user.errors, status: :unprocessable_entity }
    end
  end

  # PATCH/PUT /admin/twitter_users/1
  # PATCH/PUT /admin/twitter_users/1.json
  def update
    respond_to do |format|
      if @twitter_user.update(twitter_user_params)
        format.html { redirect_to [:admin, @twitter_user], notice: 'twitter_user was successfully updated.' }
        format.json { render :show, status: :ok, location: @twitter_user }
      else
        format.html { render :edit }
        format.json { render json: @twitter_user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/twitter_users/1
  # DELETE /admin/twitter_users/1.json
  def destroy
    @twitter_user.destroy
    respond_to do |format|
      format.html { redirect_to admin_twitter_users_url, notice: 'twitter_user was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def fetch_data
    @error = nil
    TwitterFetcher.fetch! do |error, user|
      @error = "#{error.to_s} (#{user.name})"
      break
    end

    render :layout => false

  rescue Exception => e
    @error = e
    Rails.logger.error "#{e.class} - #{e.to_s}"
    Rails.logger.error e.backtrace.join("\n")

    render :layout => false
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_twitter_user
      @twitter_user = TwitterUser.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def twitter_user_params
      params.require(:twitter_user).permit(:name, :enabled, :category_id)
    end
end
