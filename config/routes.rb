Rails.application.routes.draw do

  namespace :admin do
    resources :questions
    resources :twitter_users do
      collection do
        get 'fetch_data' => 'twitter_users#fetch_data'
      end
    end
    resources :categories
    resources :collections
    resources :settings
    resources :tags
    resources :users, :only => [:index]
    resources :images do
      member do
        delete :remove_tag
      end
    end
    
    root 'root#index'
    get '/reset_all_votes_current_user' => 'root#reset_all_votes_current_user'
    get '/reset_email_current_user' => 'root#reset_email_current_user'
  end

  namespace :api do
    get '/downloader' => 'downloader#show'
    
    get 'tagger/images' => 'tagger#images'
    get 'tagger/finished' => 'tagger#finished'
    match 'tagger/vote' => 'tagger#vote', via: [:options]
    if Rails.env.development?
      get 'tagger/vote' => 'tagger#vote'
      post 'tagger/vote' => 'tagger#vote'
    else
      post 'tagger/vote' => 'tagger#vote'
    end

    get 'categories' => 'categories#index'
    get 'categories/:id' => 'categories#show'
    get 'collections' => 'collections#index'
    get 'collections/finished' => 'collections#finished'
    get 'collections/:id/images' => 'collections#images'
    get 'collections/twitter/:name' => 'collections#twitter'

    put 'users/add_email' => 'users#add_email'
    match 'users/add_email' => 'users#add_email', via: [:options]
    get '/twitter/retweet/:image_id' => 'twitter#retweet', constraints: {image_id: /\d+/}
    get '/twitter/fav/:image_id' => 'twitter#fav', constraints: {image_id: /\d+/}
  end

  resources :image_datas, :only => [:show]
  match 'twitter/images' => 'twitter#images', via: [:get]

  get 'i/:id' => "api/downloader#show"
  get '/auth/twitter/callback', to: 'api/twitter#create'
  get '/:twitter_name', to: redirect('#/t/%{twitter_name}'), constraints: {twitter_name: /@.*/}

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
