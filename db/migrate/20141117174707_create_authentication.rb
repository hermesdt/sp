class CreateAuthentication < ActiveRecord::Migration
  def change
    create_table :authentications do |t|
      t.integer :user_id
      t.string :provider
      t.json :data

      t.timestamps
    end
  end
end
